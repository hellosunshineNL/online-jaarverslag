jQuery.extend({

    sceneAnim: function(holder){

        // PUBLIC
        var _this               = this;

        var _private = {

            // PRIVATE OBJECTS
            holder                  : _this,
            scene_1                 : null,
            parallax_1              : null,
            scene_2                 : null,
            parallax_2              : null,
            

            // PRIVATE FUNCTIONS
            setup:function()
            {   
                _private.holder             =  $(holder);

            },
            eventListeners:function() {

                $(document).on( "moveLeft", function( event ) {

                    var _activeScene = event.activeScene;

                    _private.animationLeft(_activeScene);
                });

                $(document).on( "moveRight", function( event ) {
                    
                    var _activeScene = event.activeScene;

                    _private.animationRight(_activeScene);
                });
            },
            animationLeft:function(_activeScene) {

              var activeScene = _activeScene;
              var animationDirection = "left";

              // Hide Triggers
              _private.holder.find(".m-text-modal__trigger").removeClass("animated");

              // Slide Background in
              setTimeout(function() {
                  _private.slideAnimation(activeScene, animationDirection);
              }, 500);

              // Flip Main object in
              setTimeout(function() {
                  _private.flipAnimation(activeScene);
              }, 900);

              // Animate Foreground in to center
              setTimeout(function() {
                  _private.centerAnimation(activeScene);
              }, 1000);

              // Update and animate in Triggers
              // setTimeout(function() {

              //   $(document).trigger({
              //       type: "updateTextModals"
              //   });

              //   _private.showTriggers(activeScene);
              // }, 1600);

              // Start Bubbles
              setTimeout(function() {

                $(document).trigger({
                    type: "bubbles",
                    activeScene: activeScene
                });

              }, 2000);

              // Remove body animating class
              setTimeout(function() {

                 $("body").removeClass("animating");
              }, 2200);

              

            },
            animationRight:function(_activeScene) {

                var activeScene = _activeScene;
                var animationDirection = "right";

                // Hide Triggers
                _private.holder.find(".m-text-modal__trigger").removeClass("animated");

                // Slide Background in
                setTimeout(function() {
                      _private.slideAnimation(activeScene , animationDirection);
                  }, 500);

                // Flip Main object in
                setTimeout(function() {
                    _private.flipAnimation(activeScene);
                }, 900);

                // Animate Foreground in to center
                setTimeout(function() {
                    _private.centerAnimation(activeScene);
                }, 1000);

                // Update and animate in Triggers
                // setTimeout(function() {

                //   $(document).trigger({
                //       type: "updateTextModals"
                //   });

                //   _private.showTriggers(activeScene);
                // }, 1600);

                // Start Bubbles
                setTimeout(function() {

                  $(document).trigger({
                    type: "bubbles",
                    activeScene: activeScene
                  });
                }, 2000);

                // Remove body animating class
                setTimeout(function() {
                   $("body").removeClass("animating");
                }, 2200);

            },
            slideAnimation:function(activeScene , animationDirection) {
                 var active = activeScene;
                 var direction = animationDirection;

                 if (direction == "left") {
                    $(document).find(".slide-container").removeClass("bounceInRight").removeClass("bounceInLeft");
                    $(document).find("#scene_" + active).find(".slide-container").addClass("bounceInRight");
                 }
                 else {
                    $(document).find(".slide-container").removeClass("bounceInLeft").removeClass("bounceInRight");
                    $(document).find("#scene_" + active).find(".slide-container").addClass("bounceInLeft");
                 }
            },
            flipAnimation:function(activeScene) {
               active = activeScene;
               $(document).find(".flip-container__inner").removeClass("animated");
               $(document).find("#scene_" + active).find(".flip-container__inner").addClass("animated");
            },
            centerAnimation:function(activeScene) {
                 active = activeScene;
                 $(document).find(".center-container").removeClass("animated");
                 $(document).find("#scene_" + active).find(".center-container").addClass("animated");
            },
            showTriggers:function(activeScene) {
               active = activeScene;

               var durationDefault = 150;
               var duration = 0;
               var count = 1;

              _private.holder.find("#scene_" + active).find(".m-text-modal__trigger").each(function(_index, _ell) {
                  
                  duration = parseInt(durationDefault * count);
                  setTimeout(function() {
                    $(_ell).addClass("animated");
                  }, duration);

                  count++;
              });

            },
            enableParallax:function() {
               //_private.parallax_1.enable();
            },
            disableParallax:function() {
               //$fn.parallax_1.disable();
           }

        };
        function initialize()
        {
            _private.setup();
            _private.eventListeners();
           
    
        }
        $(document).ready(function()
        {
            initialize();
        });

        // WINDOW RESIZE
        function doResize() {

        }

        var resizeTimer;
        $(window).resize(function() {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(doResize, 200);

        });
    }
});
$(function() {
    $( document ).ready(function()
    {
            var sceneAnim = new $.sceneAnim($("#m-scenes"));
    });
});
