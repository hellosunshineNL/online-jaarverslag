jQuery.extend({

    sceneNav: function(holder){

        // PUBLIC
        var _this               = this;

        var _private = {

            // PRIVATE OBJECTS
            holder                  : _this,
            
            activeScene             : null,
            firstScene              : null,
            lastScene               : null,

            buttonNext              : null,
            buttonPrev              : null,

            holderNav               : null,
            holderTimeline          : null,
            timeline                : null,

            windowW                 : null,
            windowH                 : null,
            marginLeft              : null,  
            marginTop               : null,             

            // PRIVATE FUNCTIONS
            setup:function()
            {   
                _private.holder             =  $(holder);

                //Scenes
                _private.firstScene         = _private.holder.find('#m-scene-1');
                _private.scenesCount        = _private.holder.find("> .m-scene").length;
                _private.lastScene          = _private.scenesCount;
                _private.activeScene        = 1;

                // Prev & Next Buttons
                _private.buttonNext         = $(document).find('#o-btn-next');
                _private.buttonPrev         = $(document).find('#o-btn-prev');

                // Timeline
                _private.holderNav          = $(document).find(".m-timeline__items");
                _private.holderTimeline     = $(document).find(".m-timeline");
                _private.timeline           = _private.holderTimeline.find(".m-timeline__line--active");

            },
            clickListeners:function() {
                _private.buttonPrev.bind("click", _private.leftClicked);
                _private.buttonNext.bind("click", _private.rightClicked);
                _private.holderTimeline.bind("click", _private.holderTimelineClicked);
            },
            setSceneProportions:function() {

                //Window width and height for scene scaling
                _private.windowW = $(document).outerWidth(); 
                _private.windowH = $(document).outerHeight();

                if ( _private.windowW > _private.windowH) {

                    var stageW = parseInt(_private.windowW * 0.9);
                    var stageH = parseInt(stageW * 0.75);

                    if (stageH > (_private.windowH - 60)) {
                        var stageW = parseInt(_private.windowW * 0.8);
                        var stageH = parseInt(stageW * 0.75);

                        if (stageH > (_private.windowH - 60)) {
                            var stageW = parseInt(_private.windowW * 0.7);
                            var stageH = parseInt(stageW * 0.75);

                            if (stageH > (_private.windowH - 60)) {
                                var stageW = parseInt(_private.windowW * 0.6);
                                var stageH = parseInt(stageW * 0.75);

                                if (stageH > (_private.windowH - 60)) {
                                    var stageW = parseInt(_private.windowW * 0.5);
                                    var stageH = parseInt(stageW * 0.75);
                                }
                            }
                        }
                    }
                     
                    var marginLeft = parseInt(stageW / 2);
                    var marginTop = parseInt(stageH / 2);

                    $(document).find(".m-scene__inner").css("width" , "" + stageW + "px").css("height" , "" + stageH + "px");
                    $(document).find(".m-scene__inner").css("margin-left" , "-" + marginLeft + "px").css("margin-top" , "-" + marginTop + "px");

                }
                else {

                    var stageW = parseInt(_private.windowW * 1.2);
                    var stageH = parseInt(stageW * 0.75);
                    
                    if (_private.windowW < 400) {
                        var stageW = parseInt(_private.windowW * 1.4);
                        var stageH = parseInt(stageW * 0.75);
                    }
                    else if (_private.windowW >= 400 && _private.windowW < 600) {
                        var stageW = parseInt(_private.windowW * 1.3);
                        var stageH = parseInt(stageW * 0.75);
                    }

                    var marginLeft = parseInt(stageW / 2);
                    var marginTop = parseInt(stageH / 2);

                    $(document).find(".m-scene__inner").css("width" , "" + stageW + "px").css("height" , "" + stageH + "px");
                    $(document).find(".m-scene__inner").css("margin-left" , "-" + marginLeft + "px").css("margin-top" , "-" + marginTop + "px");
                }

                 // Set last scene heights
                    $(document).find(".m-scene--final__content-bottom.for-height").remove();

                    var html = $(document).find(".m-scene--final__content-bottom").html();
                    $("body").append("<div class='m-scene--final__content-bottom for-height' style='position:absolute; top: 0; left: 0; width:" + _private.windowW + "px'>" + html + "</div>");
                    
                   var  h = $(document).find(".m-scene--final__content-bottom.for-height").outerHeight();
                   var hContent = _private.windowH;
                

                    if(Modernizr.mq('(min-width: 768px)')) {
                        hContent = parseInt(hContent - 160);
                    }
                    else if(Modernizr.mq('(min-width: 991px)')) {
                        hContent = parseInt(hContent - 180);
                    }
                    else {
                        hContent = parseInt(hContent - 120);
                    }

                    restH = parseInt(hContent - h);

                    $(document).find(".m-scene--final__bubbles").css("height" , "" + restH + "px")
                
            },
            setupNavTimeline:function() {
                
                _private.holder.find(".m-scene").each(function(_index, _ell) {
                    var target = $(this).attr("data-scene");
                    var leftPos = $(this).attr("data-timeline-pos");
                    var title   = $(this).attr("data-title");

                    if ($(this).hasClass("m-scene--final")) {
                        _private.holderNav.append("<div id='scene-link_" +  target + "' data-target='" + target + "' class='m-timeline__item txt--white  m-timeline__item--final o-tooltip' style='left:" + leftPos + " ;' data-tooltip='"+title+"' data-placement='top'><div class='m-timeline__item__dot'></div></div>");
                    }
                    else {
                        _private.holderNav.append("<div id='scene-link_" +  target + "' data-target='" + target + "' class='m-timeline__item txt--white o-tooltip' style='left:" + leftPos + " ;' data-tooltip='"+title+"' data-placement='top'><div class='m-timeline__item__dot'></div></div>");
                    }
                    
               });

                _private.holderNav.find("#scene-link_1").addClass("is--active");

            },
            navTimelineListeners:function() {
               _private.holderNav.find(".m-timeline__item").each(function(_index, _ell) {
                    $(this).bind("click", _private.goToSlide);
               });
            },
            setProperties:function() {
               _private.hideLeft();
               _private.moveLeft();
            },
            holderTimelineClicked:function() {
                _private.holderTimeline.toggleClass("is--active");

                $(document).mouseup(function(e) 
                {
                    if (!_private.holderTimeline.is(e.target) && _private.holderTimeline.has(e.target).length === 0) 
                    {
                        _private.holderTimeline.removeClass("is--active");
                    }
                });
                
                $(document).trigger({
                	type: "sceneChanged",
                    activeScene: _private.activeScene
            	});
            },
            leftClicked:function() {

                if (_private.activeScene > 1) {
                    _private.activeScene--;

                    if (_private.activeScene == 1) {
                        _private.hideLeft();
                    }
                    if (_private.activeScene < _private.lastScene) {
                        _private.showRight();
                    }

                    _private.moveRight();
                }
            },
            rightClicked:function() {

                if (_private.activeScene < _private.lastScene) {
                     _private.activeScene++;

                    if (_private.activeScene > 1){
                        _private.showLeft();
                    }
                    if (_private.activeScene == _private.lastScene) {
                        _private.hideRight();
                    }

                    _private.moveLeft();
                }          
            },
            moveLeft:function() {               

                $("body").addClass("animating");

                // Update timeline
                _private.updateTimeline();

                //Hide old visible scene
                _private.holder.find(".m-scene.is--active").attr("data-animation" , "slide-left-out");
                _private.holder.find(".m-scene.is--active").addClass("is--old");

                setTimeout(function() {
                    _private.holder.find(".m-scene.is--old").removeClass("is--active").removeClass("is--old");

                    //Reset bubbles
                     $(document).find(".m-bubbles__visible").empty();
                     $(document).find(".m-bubbles__hidden").find(".m-bubbles__item").removeClass("is--active").removeClass("is--visible");

                }, 950);

                //Show new active scene
                _private.holder.find("#m-scene-" + _private.activeScene).attr("data-animation" , "slide-right");
                _private.holder.find("#m-scene-" + _private.activeScene).addClass("is--active");

                //Trigger in-scene animations
                $(document).trigger({
                    type: "moveLeft",
                    activeScene: _private.activeScene,
                });

                setTimeout(function() {
                    $("body").removeClass("animating");
                }, 1000);
                
            },
            moveRight:function() {             

                $("body").addClass("animating");

                // Update timeline
                _private.updateTimeline();

                //Hide old visible scene
                _private.holder.find(".m-scene.is--active").attr("data-animation" , "slide-right-out");
                _private.holder.find(".m-scene.is--active").addClass("is--old");

                setTimeout(function() {
                    _private.holder.find(".m-scene.is--old").removeClass("is--active").removeClass("is--old");
                }, 950);

                //Show new active scene
                _private.holder.find("#m-scene-" + _private.activeScene).attr("data-animation" , "slide-left");
                _private.holder.find("#m-scene-" + _private.activeScene).addClass("is--active");
                
                //Trigger in-scene animations
                $(document).trigger({
                    type: "moveRight",
                    activeScene: _private.activeScene
                });

                setTimeout(function() {
                    $("body").removeClass("animating");
                }, 1000);

            },
            updateTimeline:function() {
                
                _private.holderTimeline.find(".m-timeline__item.is--active").removeClass("is--active");

                var timelineLeft = _private.holder.find("#m-scene-" + _private.activeScene).attr("data-timeline-pos");
                _private.timeline.css("width" , timelineLeft);

       
                if (_private.holderTimeline.find("#scene-link_" + _private.activeScene).hasClass("m-timeline__item--final")) {
                    _private.holderTimeline.addClass("is--final");
                }
                else {
                    _private.holderTimeline.removeClass("is--final");
                }
                setTimeout(function() {
       
                    _private.holderTimeline.find("#scene-link_" + _private.activeScene).addClass("is--active");

                }, 1050);
                
                $(document).trigger({
                	type: "sceneChanged",
                    activeScene: _private.activeScene
            	});
            },
            goToSlide:function() {

                if(!$("body").hasClass("animating")) {

                    var target  = $(this).attr("data-target");

                    // Hide & show controls
                    if (target == 1 ) {
                        _private.hideLeft();
                        _private.showRight();
                    }
                    else if (target == _private.lastScene) {
                        _private.hideRight();
                        _private.showLeft();
                    }
                    else {
                        _private.showLeft();
                        _private.showRight();
                    }

                    // Move left or right
                    if (_private.activeScene > target ) {
                        _private.activeScene = $(this).attr("data-target");
                        _private.moveRight();
                    }
                    else if (_private.activeScene < target) {
                        _private.activeScene = $(this).attr("data-target");
                        _private.moveLeft();
                    }
                    else {

                    }    
                }   
            },
            showLeft:function() {
                 _private.buttonPrev.animate({
                    left: "15",
                    opacity: '1'
                  }, 200, function() {
                    
                  });
            },
            hideLeft:function() {
                _private.buttonPrev.animate({
                    opacity: 0,
                    left: "-=15",
                    }, 200, function() {
                  
                });
            },
            showRight:function() {
                _private.buttonNext.animate({
                    opacity: 1,
                    right: "15",
                    }, 200, function() {
                  
                });
            },
            hideRight:function() {
                _private.buttonNext.animate({
                    opacity: 0,
                    right: "-=15",
                    }, 200, function() {
                  
                });
            }

        };
        function initialize()
        {
            _private.setup();
            _private.setSceneProportions();
            _private.setProperties();
            _private.clickListeners();
            _private.setupNavTimeline();
            _private.navTimelineListeners();
            
           
        }
        $(document).ready(function()
        {
            initialize();
        });

        // WINDOW RESIZE
        function doResize() {
            _private.setSceneProportions();
        }

        var resizeTimer;
        $(window).resize(function() {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(doResize, 200);

        });
    }
});
$(function() {
    $( document ).ready(function()
    {
            var sceneNav = new $.sceneNav($("#m-scenes"));
    });

});