 jQuery.extend({

    mBubbles: function(holder){

        // PUBLIC
        var _this               = this;

        var _private = {

            // PRIVATE OBJECTS
            holder                  : _this,

            holderContainer         : null,
            speechBubble            : null,
            countBubbles            : null,
            speechBubble            : null,
            
            holderVisible           : null,
            holderHidden            : null,
            
            holderBubblesVisible	: null,
            holderBubblesHidden		: null,
            holderBubblesAnimator	: null,
            holderNames				: null,
            bubbles					: null,
            activeScene				: null,
            hasTitle				: false,
			holderTitle				: null,
            
            delayStart              : 0,
            delayEnd              	: 0,
            intervalStep			: 20,
            animatorOffSetTop		: 100,
            

            // PRIVATE FUNCTIONS
            setup:function()
            {   
	            _private.holder             = holder;
				
				_private.holderContainer    = _private.holder.find('.m-bubbles__container');
                _private.speechBubble       = _private.holderContainer.find('.m-bubbles__item');
                _private.countBubbles       = _private.speechBubble.length;
                _private.delayStart		    = _private.holder.data('delay-start');
                _private.delayEnd		    = _private.holder.data('delay-end');
                _private.showProgressbar    = _private.holder.data('progress');
                _private.titleBubble        = _private.speechBubble.data('title');
                
                if(_private.holder.data("names"))
                {
	                _private.holderNames 	= _private.holder.find('.m-bubbles__item__content__names');
	                
	                var url      = window.location.href;
	                // var url			= 'http://jeroendevries.enzuyderland.nl';
  			
					url = url.replace('https://www.','');
					url = url.replace('http://www.','');
		  			url = url.replace('http://','');
		  			url = url.split(".")[0];
		  			
		  			if(url.length > 0 && url != "enzuyderland" && url != "jouwnaam" && url != "jouw-naam")
		  			{
			  			_private.holderNames.data('name-primair', capitalizeFirstLetter(url));
		  			}else{
			  			_private.holderNames.data('name-primair', capitalizeFirstLetter("jij"));
		  			}
                }
            },
            reposition:function()
            {
	            var newTop 		= _private.holderBubblesVisible.height();
	            var topDelta	= 0;
	            
	            _private.holderBubblesAnimator.children().each(function(index, element)
	            {
		            topDelta += ($(element).height() + Number($(element).css("margin-bottom").substring(0, $(element).css("margin-bottom").length - 2)));
	            });
	            
	            newTop -= topDelta;
				
				_private.holderBubblesAnimator.css("top", newTop);

				if($(_private.holderBubblesAnimator).find(".is--on-top").children().length > 0)
				{
					_private.holderBubblesAnimator.css("top", 0);
				}
            },
            startSequence:function()
            {
	            _private.holder					= $(document).find("#scene_" + _private.activeScene).find(".m-bubbles");
	            _private.holderBubblesVisible	= $(document).find("#scene_" + _private.activeScene).find('.m-bubbles__visible');
	            _private.holderBubblesHidden	= $(document).find("#scene_" + _private.activeScene).find('.m-bubbles__hidden');
	            _private.bubbles				= _private.holderBubblesHidden.find(".m-bubbles__item");
	        	_private.nBubbles 				= _private.bubbles.length;
	        	_private.nBubbleNext 			= 0;
	        	
	        	if(_private.holderBubblesHidden.children().data('data-title'))
                {
	                _private.hasTitle		= true;
	                _private.holderTitle	= _private.holderBubblesHidden.find("[data-title='true']").first();
                }
	        	
	        	if(_private.holderBubblesVisible.children().length < 1)
	        	{
		        	_private.holderBubblesVisible.append("<div class='m-bubbles__visible__animator'></div>");
		        	_private.holderBubblesAnimator = _private.holderBubblesVisible.find(".m-bubbles__visible__animator");
		        	_private.holderBubblesAnimator.css("top",_private.holderBubblesVisible.height());
	        	}
	        	
	        	_private.updateSequence();
            },
            updateSequence:function()
            {
	            _private.holderBubblesHidden.find(".is--active").removeClass('is--active');
	            _private.holderBubblesAnimator.find(".is--active").removeClass('is--active');
	            
	            // next bubble
		        var _element 		= _private.bubbles.eq(_private.nBubbleNext);
		        var _elementHeight 	= _private.getHiddenElementHeight(_element);
		            
	            if(_private.nBubbleNext < _private.nBubbles)
	            {
		            if(_element.data('delay-start'))
		            {
			            setTimeout(function() 
			            {
				            _update();
							
			            }, _element.data('delay-start'));
		            }else{
			            _update();
		            }
	            }else{
		            if(_private.holder.data('loop'))
		            {
			            _private.nBubbleNext = 0;
			        	
			        	_private.updateSequence();
		            }else{
			            // no more bubbles - end sequence
						_private.endSequence();
		            }
	            }
	            
	            _private.nBubbleNext++;
	            
	            function _update()
	            {
		            _private.showBubble(_element);
				           			            
				    _private.moveBubblesUp(_elementHeight + Number(_element.css("margin-bottom").substring(0, _element.css("margin-bottom").length - 2)));
				    
				    _private.hideBubbles();
	            }
            },
            endSequence:function()
            {
	           	if(_private.holder.data('delay-end'))
		        {
			        if(_private.holder.data('delay-end') != 'infinite')
			        {
				        setTimeout(function() 
				        {
					        _private.moveBubblesOut();
					        
					        // event ended
					        $(document).trigger({
		                    	type: "updateTextModals",
			                    activeScene: _private.activeScene
		                	});
					        
					    }, _private.holder.data('delay-end'));
			        }
				}else{
					_private.moveBubblesOut();
					
					// event ended
					$(document).trigger({
                    	type: "updateTextModals",
	                    activeScene: _private.activeScene
                	});
				}
				
				if(_private.activeScene == 1)
				{
					$(document).trigger({
	                	type: "notificateOnboarding-intro",
	                    activeScene: _private.activeScene
	            	});
	            	
	            	setTimeout(function() {
		            	$(document).trigger({
		                	type: "notificateOnboarding-timeline",
		                    activeScene: _private.activeScene
		            	});
		            }, 3000);
				}
            },
            showBubble:function(element)
            {
	            _private.holderBubblesHidden.find(".is--visible").removeClass('is--visible');
	            
	        	element.addClass('is--visible is--active');
	        	
	        	var _element 		= element.clone().appendTo(_private.holderBubblesAnimator);
	        	
	        	if(_private.showProgressbar)
	        	{
		        	setTimeout(function()
		        	{
			        	_private.setupProgressBar(_element)
			        }, 500);
	        	}
	        	
	        	// intro animation - names
	        	if(element.hasClass('m-bubbles__item--names'))
	        	{
		        	var _arrayNames 		= _private.holderNames.data("names").split(", ");
		        	var _nNames				= _private.holderNames.data("names-length");
		        	var _durationName		= _private.holderNames.data("name-duration");
		        	var _nNameAnimated		= 0;
		        	var _arrayNamesChosen	= [];
		        	var _randomIndex		= 0;
		        	var _holderNamesVisible	= _private.holderBubblesVisible.find(".m-bubbles__item__content__names");
		        	var _heightContainer	= _private.holderNames.height();
		        	
		        	for (i = 0; i < Number(_nNames); i++) 
		        	{
			        	_randomIndex = Math.floor(getRandomArbitrary(0,_arrayNames.length));
			        	_arrayNamesChosen.push(_arrayNames[_randomIndex]);
			        	_arrayNames.splice(_randomIndex, 1);
		        	}
		        	
		        	if(_private.holderNames.data('name-primair') != "undefined")
		        	{
			        	_nNames++;
			        	
			        	_arrayNamesChosen.unshift(_private.holderNames.data('name-primair'));
		        	}
		        	
		        	$.each(_arrayNamesChosen, function(index, name)
		        	{
			        	var _name = _holderNamesVisible.find(".names__animator").prepend("<h2>"+name+"</h2>");
		        	});
		        	
		        	var _nameHeight			= _holderNamesVisible.find(".names__animator").children().first().height();
		        	
		        	_private.holderBubblesVisible.find(".m-bubbles__item__content__names").find(".container--static").css("top", _nameHeight + 30);
		        	_private.holderBubblesVisible.find(".m-bubbles__item__content__names").css("height", _nameHeight + _private.holderBubblesVisible.find(".m-bubbles__item__content__names").find(".container--static").height() + 25);
		        	_private.holderBubblesVisible.find(".m-bubbles__item__content__names").find(".names__animator").find("h2").css("opacity", 0);
		        	_private.holderBubblesVisible.find(".m-bubbles__item__content__names").find(".names__animator").find("h2").eq(0).css("opacity", 1);
		        	
		        	_holderNamesVisible.find("h2").last().remove();
		        	
		        	// animate through names
		        	
		        	var _timerNames = setInterval(function()
		        	{
			        	_nNameAnimated++;
			        	
			        	if(_nNameAnimated > _nNames)
			        	{
				        	clearInterval(_timerNames);
			        	}else{
				        	var _namesAnimator 	= _holderNamesVisible.find(".names__animator");
				        	var _newTop 		= _namesAnimator.position().top - 65;
				        	
				        	if(_nNameAnimated < _nNames)
							{
					        	_namesAnimator.animate({
									top:_newTop
								}, 250);
							}
							
				        	_holderNamesVisible.find("h2").eq(_nNameAnimated).animate({
								opacity:1
							}, 250);
							
							if(_nNameAnimated < _nNames)
							{
								_holderNamesVisible.find("h2").eq(_nNameAnimated-1).animate({
									opacity:0
								}, 250);
							}
			        	}
		        	}, _durationName);
	        	}
            },
            hideBubbles:function(element)
            {
	            if(_private.holderBubblesAnimator.children().length > 0)
	            {
		            _private.holderBubblesAnimator.children().each(function(index, element)
		            {
			            var _elementHeight 			= $(element).height();
				        var _elementTop				= $(element).position().top;
				        var _elementOffsetTop		= $(element).offset().top;
				        var _animatorTop			= _private.holderBubblesAnimator.position().top;
				           	 
				        if((_elementOffsetTop - _elementHeight) < _private.animatorOffSetTop)
				        {
					        $(element).animate({
								opacity:0
							}, 500, function()
							{
								// 
							});
				        }
			           	 
		            });
	            }
            },
            moveBubblesUp:function(yDelta)
            {
	            var _currentTopPx 					= $(_private.holderBubblesAnimator).css("top");
	            var _currentTop						= Number(_currentTopPx.substring(0, _currentTopPx.length - 2));
				var _newTop 						= _currentTop - yDelta;
				var _intervalTitle;
	            
	            $(_private.holderBubblesAnimator).animate({
				  top:_newTop
				}, 500);
				
				if(_private.hasTitle)
				{
					clearInterval(_intervalTitle);
					
					_intervalTitle = setInterval(function()
					{
						var _offsetTop 				= _private.holderTitle.offset().top;
						var _animatorTop			= _private.holderBubblesAnimator.position().top;
					}, 10);
				}
            },
            moveBubblesOut:function()
            {

	            var _firstElement 					= _private.holderBubblesAnimator.children().first();
	            var _lastElement 					= _private.holderBubblesAnimator.children().last();
	            
	            if(_firstElement.data("title"))
	            {
		            if(_lastElement.data('delay-end'))
			        {
				        setTimeout(function() 
				        {
					        _firstElement.addClass("is--on-top");
					        
							$(_firstElement).animate({
							  opacity:1
							}, 500);
					    }, 500);
					}else{
						_firstElement.addClass("is--on-top");
						
						$(_firstElement).animate({
						  opacity:1
						}, 500);
					}
	            }
	            
	            $(_private.holderBubblesAnimator).animate({
				  top:0
				}, 500);
				
				_private.holderBubblesAnimator.children().each(function(index, element)
				{
					var _delay = (_private.holderBubblesAnimator.children().length - index) * 100;
					
					if($(element).data("title") != "true" && $(element).data("title") != true)
					{
						$(element).delay(_delay).animate({
						  opacity:0
						}, 500);
					}
				});
				
            },
            setupProgressBar:function(element)
            {
	            var _progressBarHolder 		= element.find(".m-bubbles__item__progressbar");
	            var _progressBar			= _progressBarHolder.children("div");
	            var _duration				= element.data("duration");
	            var _t 						= 0;
	            
	            var _intervalProgressBar 	= setInterval(function()
	            {
		            _t += _private.intervalStep;
		            
		            if(_t > _duration)
		            {
			            _t = _duration;
		            }
					
		            var _progress = (_t/_duration)*100 + "%";
		            
		            _private.updateProgressBar(_progressBar, _progress);
		            
		            if(_t >= _duration)
		            {
			        	clearInterval(_intervalProgressBar);
			        	element.closest(".m-bubbles__item").addClass("fade-to-inactive");
			        	
			        	var _sceneAvailable 		= false;
			        	var _scene;
			        	var _sceneNr				= 0;
			        	
			        	if(element.closest(".scene").children().length > 0)
			        	{
				        	_sceneAvailable 	= true;
				        	_scene 				= element.closest(".scene").attr("id");
				        	_sceneNr 			= _scene.substr(6, _scene.length);
			        	}
			        	
			        	if(_sceneAvailable)
			        	{
				        	if(_sceneNr == _private.activeScene)
				        	{
					        	_private.updateSequence();
				        	}
				        }
		            }
	            }, _private.intervalStep); 
            },
            updateProgressBar:function(progressBar, progress)
            {
	            progressBar.css("width", progress);
            },
            getHiddenElementHeight:function(_element)
            {
	            _private.holderBubblesHidden.addClass("get__height");
	        	
	        	var hiddenElementHeight = _element.height();
	        	
	        	_private.holderBubblesHidden.removeClass("get__height");
	        	
	        	return hiddenElementHeight;
            },
            eventListeners:function() 
            {
                $(document).on( "bubbles", function( event ) {
                    
                    _private.activeScene = event.activeScene;
                    _private.startSequence();
                });
            }
        };
        function capitalizeFirstLetter(string) {
		    return string.charAt(0).toUpperCase() + string.slice(1);
		}
		function getRandomArbitrary(min, max) {
		  return Math.random() * (max - min) + min;
		}
        function initialize()
        {
            _private.setup();   
            _private.eventListeners();
        }
        $(document).ready(function()
        {
            initialize();
        });

        // WINDOW RESIZE
        function doResize() {
            _private.reposition();
        }

        var resizeTimer;
        $(window).resize(function() 
        {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(doResize, 500);
        });
    }
});
$(function() {
    $( document ).ready(function()
    {
            var mBubbles = new $.mBubbles($(".m-bubbles"));
    });
});