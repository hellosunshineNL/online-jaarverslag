jQuery.extend({

    mTextModals: function(holder){

        // PUBLIC
        var _this               = this;

        var _private = {

            // PRIVATE OBJECTS
            holder                  : _this,
            modal                   : null,
            modalInner              : null,
            closeBtn                : null,

            // PRIVATE FUNCTIONS
            setup:function() 
            {
                _private.trigger = $(holder).find(".m-text-modal__trigger");
                _private.modal = $(holder).find(".m-text-modal__modal");

            },
            setupPosition:function(modalWrapper) 
            {
                 $(holder).find(".m-text-modal").each(function(_index, _ell) {
                    var modal = $(this).find(".m-text-modal__modal");
                    var modalInner = modal.find(".m-text-modal__modal__inner");
                    var trigger = modal.find(".m-text-modal__trigger");

                    var modalWidth = modal.outerWidth();
                    var modalHeight = modal.outerHeight();

                    modal.attr('data-width',modalWidth);
                    modal.attr('data-height',modalHeight);
                });

            },
            eventListeners:function() 
            {
                $(document).on( "updateTextModals", function( event ) {

                    activeScene = event.activeScene;
                    
                    _private.setupPosition();
                    _private.closeOtherModals();

                    setTimeout(function() 
                    {
                        _private.showTriggers(activeScene);
                    }, 500);

                });
            },
            addTriggerClickListener:function() 
            {

                $(holder).find(".m-text-modal").each(function(_index, _ell) {
                    
                    var textModal = $(this);
                    var trigger = textModal.find(".m-text-modal__trigger");

                    trigger.unbind().bind('click', function() {
                    
                        if (!$(this).hasClass("open")) {
                            _private.updateModals(textModal);
                            //_private.showModal(textModal);
                        }
                        else {
                            _private.closeModal(textModal);

                        }
                    });

                });

            },
            addTriggerClickListenerMobile:function() 
            {

                $(holder).find(".m-text-modal").each(function(_index, _ell) {
                   
                    var textModal = $(this);
                    var trigger = textModal.find(".m-text-modal__trigger");
                    var modal = textModal.find(".m-text-modal__modal");
                    var modalInner = $(this).find(".m-text-modal__modal__inner");

                    trigger.unbind().bind('click', function() {
                        
                        if (!_private.modal.hasClass("open")) {
                            modal.addClass("open");
                           trigger.addClass("no-pulse");

                            // var modalHeight = modal.attr('data-height');
                            // var modalTop = parseInt(modalHeight / 2);

                            // modal.css("margin-top", "-" + modalTop + "px");

                           modalInner.delay(100).animate({
                                opacity: 1
                            }, 300);

                        }
                        else {
                            modal.removeClass("open");
                            trigger.removeClass("no-pulse");

                            modalInner.delay(100).animate({
                                opacity: 0
                            }, 300);
                        }
                    });

                });

            },
            clickCloseListener:function() 
            {
                $(holder).find(".m-text-modal").each(function(_index, _ell) {

                    var textModal = $(this);
                    var closeBtn = textModal.find(".m-text-modal__close");


                    closeBtn.unbind().bind('click', function() {
                        _private.closeModal(textModal);
                    });

                });


            },
            clickCloseListenerMobile:function() 
            {   
                $(holder).find(".m-text-modal").each(function(_index, _ell) {

                    var modal = $(this).find(".m-text-modal__modal");
                    var modalInner = $(this).find(".m-text-modal__modal__inner");
                    var trigger = $(this).find(".m-text-modal__trigger");
                    var closeBtn = $(this).find(".m-text-modal__close");


                    closeBtn.unbind().bind('click', function() {
                        modal.removeClass("open");
                        trigger.removeClass("no-pulse");
                    });

                });

            },
            
            closeModal:function(textModal) {

                var modal = textModal.find(".m-text-modal__modal");
                var modalInner = textModal.find(".m-text-modal__modal__inner");
                var trigger = textModal.find(".m-text-modal__trigger");

                modalInner.animate({
                    opacity: 0
                }, 300);

                modal.animate({
                    width: 0,
                    height: 0
                  }, {
                    duration: 500,
                    specialEasing: {
                      width: "swing",
                      height: "swing"
                    },
                    complete: function() {
                       modal.removeClass("open");
                       trigger.removeClass("no-pulse");
                    }
                });

                modal.css("width", "");
                modal.css("height", "");

            },
            closeOtherModals:function() {

                $(document).find(".m-text-modal__modal").each(function(_index, _ell)
                {
                    $(this).removeClass("open");
                    $(this).find(".m-text-modal__modal__inner").css("opacity" , "0");
                    $(this).css("width", "");
                    $(this).css("height", "");
                });

                $(document).find(".m-text-modal__trigger").each(function(_index, _ell)
                {
                    $(this).removeClass("no-pulse");
                });

            },
            showTriggers:function(activeScene) {

               active = activeScene;

               var durationDefault = 150;
               var duration = 0;
               var count = 1;

               $(holder).find("#scene_" + active).find(".m-text-modal__trigger").each(function(_index, _ell) {
                  
                  duration = parseInt(durationDefault * count);
                  setTimeout(function() {
                    $(_ell).addClass("animated");
                  }, duration);

                  count++;
              });

            },
            updateModals:function(textModal) {
                var textModal = textModal;
                var trigger = textModal.find(".m-text-modal__trigger");

                // Setup Position classes
                var windowW = $(window).outerWidth();
                var windowH = $(window).outerHeight();
                var triggerH = trigger.height();

                var topPos = $(textModal).offset().top;
                var bottomPos = windowH - topPos - triggerH;
                var leftPos = $(textModal).offset().left;
                var rightPos = windowW - leftPos - triggerH;

                if (leftPos < rightPos) {
                 $(textModal).addClass("left");
                }
                else {
                 $(textModal).addClass("right");
                }

                if (topPos < bottomPos) {
                 $(textModal).addClass("top");
                }
                else {
                 $(textModal).addClass("bottom");
                }

                _private.showModal(textModal);



            },
            showModal:function(textModal) {

                var modal = textModal.find(".m-text-modal__modal");
                var modalInner = textModal.find(".m-text-modal__modal__inner");
                var trigger = textModal.find(".m-text-modal__trigger");

                var modalWidth = modal.attr('data-width');
                var modalHeight = modal.attr('data-height');

                _private.closeOtherModals();

                modal.css("width", "0");
                modal.css("height", "0");

                modal.addClass("open");
                trigger.addClass("no-pulse");

                // Animate in
                $(modal).animate({
                    width: modalWidth,
                    height: modalHeight
                  }, {
                    duration: 500,
                    specialEasing: {
                      width: "swing",
                      height: "swing"
                    },
                    complete: function() {
                      // Complete
                    }
                  });

               modalInner.delay(300).animate({
                    opacity: 1
                }, 300);

            },
            update:function() {

                if(Modernizr.mq('(min-width: 768px)')) {
                    _private.addTriggerClickListener();
                    _private.clickCloseListener();
                }
                else {
                    _private.addTriggerClickListenerMobile();
                    _private.clickCloseListenerMobile();
                }
            }
        };

        function initialize()
        {
            

            if(Modernizr.mq('(min-width: 768px)')) {
                _private.setup();
                _private.setupPosition();
                _private.eventListeners();
                _private.addTriggerClickListener();
                _private.clickCloseListener();
            }
            else {
                _private.setup();
                _private.eventListeners();
                _private.addTriggerClickListenerMobile();
                _private.clickCloseListenerMobile();
            }
        }

        $(document).ready(function()
        {
            initialize();
        });

        // WINDOW RESIZE
        function doResize() {
            _private.update();
        }

        var resizeTimer;
        $(window).resize(function() {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(doResize, 200);

        });
    }
});

$(function() {
    $( document ).ready(function()
    {
        $("#m-scenes").each(function(_index, _ell)
        {
            var mTextModals = new $.mTextModals($(_ell));
        });
        
    });
});

