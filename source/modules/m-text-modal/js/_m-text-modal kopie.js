// jQuery.extend({

//     mTextModal: function(holder){

//         // PUBLIC
//         var _this               = this;

//         var _private = {

//             // PRIVATE OBJECTS
//             trigger                 : null,
//             modal                   : null,
//             modalInner              : null,
//             closeBtn                : null,

//             // PRIVATE FUNCTIONS
//             setup:function() 
//             {
//                 _private.trigger = $(holder).find(".m-text-modal__trigger");
//                 _private.modal = $(holder).find(".m-text-modal__modal");
//                 _private.modalInner = $(holder).find(".m-text-modal__modal__inner");
//                 _private.closeBtn = _private.modal.find(".m-text-modal__close");

//                 var activeScene = 1;
//                 _private.updatePosition(activeScene);
//             },
//             // setupPosition:function() 
//             // {
//             // 	var modalWidth = _private.modal.outerWidth();
//             //     var modalHeight = _private.modal.outerHeight();

//             //     $(holder).attr('data-width',modalWidth);
//             //     $(holder).attr('data-height',modalHeight);

//             // 	var windowW = $(".m-scene__layer__triggers").outerWidth();
//             // 	var windowH = $(".m-scene__layer__triggers").outerHeight();
//             // 	var triggerH = _private.trigger.height();

//             // 	var topPos = $(holder).offset().top;
//             // 	var bottomPos = windowH - topPos - triggerH;
//             // 	var leftPos = $(holder).position().left;
//             // 	var rightPos = windowW - leftPos - triggerH;

//             // 	if (leftPos < rightPos) {
//             // 		$(holder).addClass("left");
//             // 	}
//             // 	else {
//             // 		$(holder).addClass("right");
//             // 	}

//             // 	if (topPos < bottomPos) {
//             // 		$(holder).addClass("top");
//             // 	}
//             // 	else {
//             // 		$(holder).addClass("bottom");
//             // 	}



//             // },
//             eventListeners:function() 
//             {
//                 $(document).on( "updateTextModals", function( event ) {

//                     var activeScene = event.scene;
//                     _private.updatePosition(activeScene);
                    
//                 });
//             },
//             updatePosition:function( activeScene ) 
//             {
//                 var active = activeScene;

//                 $(document).find("#m-scene-" + active).find(".m-text-modal").each(function(_index, _ell) {

//                     var modalWidth = $(this).find(".m-text-modal__modal").outerWidth();
//                     var modalHeight = $(this).find(".m-text-modal__modal").outerHeight();

//                     $(this).attr('data-width',modalWidth);
//                     $(this).attr('data-height',modalHeight);

//                     var windowW = $(document).find("#m-scene-" + active).find(".m-scene__layer__triggers").outerWidth();
//                     var windowH = $(document).find("#m-scene-" + active).find(".m-scene__layer__triggers").outerHeight();
//                     var triggerH = _private.trigger.height();

//                     var topPos = $(this).offset().top;
//                     var bottomPos = windowH - topPos - triggerH;
//                     var leftPos = $(this).position().left;
//                     var rightPos = windowW - leftPos - triggerH;

//                     $(this).removeClass("left").removeClass("right").removeClass("top").removeClass("bottom");

//                     if (leftPos < rightPos) {
//                         $(this).addClass("left");
//                     }
//                     else {
//                         $(this).addClass("right");
//                     }

//                     if (topPos < bottomPos) {
//                         $(this).addClass("top");
//                     }
//                     else {
//                         $(this).addClass("bottom");
//                     }

//                 });
//             },
//             addTriggerClickListener:function() 
//             {
//                 _private.trigger.bind('click', function() {
                    
//                     if (!_private.modal.hasClass("open")) {
//                         _private.showModal();
//                     }
//                     else {
//                         _private.closeModal();
//                     }
//                 });

//             },
//             addTriggerClickListenerMobile:function() 
//             {
//                 _private.trigger.bind('click', function() {
                    
//                     if (!_private.modal.hasClass("open")) {
//                         _private.modal.addClass("open");
//                         _private.trigger.addClass("no-pulse");
//                     }
//                     else {
//                         _private.modal.removeClass("open");
//                         _private.trigger.removeClass("no-pulse");
//                     }
//                 });

//             },
//             clickCloseListener:function() 
//             {
//                 _private.closeBtn.bind('click', function() {
//                     _private.closeModal();
//                 });

//             },
//             clickCloseListenerMobile:function() 
//             {
//                 _private.closeBtn.bind('click', function() {
//                     _private.modal.removeClass("open");
//                     _private.trigger.removeClass("no-pulse");
//                 });

//             },
//             closeModal:function() {

//                 $(_private.modalInner).animate({
//                     opacity: 0
//                 }, 300);

//                 $(_private.modal).animate({
//                     width: 0,
//                     height: 0
//                   }, {
//                     duration: 500,
//                     specialEasing: {
//                       width: "swing",
//                       height: "swing"
//                     },
//                     complete: function() {
//                         _private.modal.removeClass("open");
//                         _private.trigger.removeClass("no-pulse");
//                     }
//                   });

//             },
//             closeOtherModals:function() {

//                 $(document).find(".m-text-modal__modal").each(function(_index, _ell)
//                 {
//                     $(this).removeClass("open");
//                     $(this).find(".m-text-modal__modal__inner").css("opacity" , "0");
//                 });

//                 $(document).find(".m-text-modal__trigger").each(function(_index, _ell)
//                 {
//                     $(this).removeClass("no-pulse");
//                 });

//             },
//             showModal:function() {
//                 w = $(holder).attr('data-width');
//                 h = $(holder).attr('data-height');

//                 _private.closeOtherModals();

//                 _private.modal.css("width", "0");
//                 _private.modal.css("height", "0");

//                 _private.modal.addClass("open");
//                 _private.trigger.addClass("no-pulse");

//                 $(_private.modal).animate({
//                     width: w,
//                     height: h
//                   }, {
//                     duration: 500,
//                     specialEasing: {
//                       width: "swing",
//                       height: "swing"
//                     },
//                     complete: function() {
//                       // Complete
//                     }
//                   });

//                $(_private.modalInner).delay(300).animate({
//                     opacity: 1
//                 }, 300);

//             },
//             update:function() {

//             }
//         };

//         function initialize()
//         {
            

//             if(Modernizr.mq('(min-width: 768px)')) {
//                 _private.setup();
//                 //_private.setupPosition();
//                 _private.eventListeners();
//                 _private.addTriggerClickListener();
//                 _private.clickCloseListener();
//             }
//             else {
//                 _private.setup();
//                 _private.addTriggerClickListenerMobile();
//                 _private.clickCloseListenerMobile();
//             }
//         }

//         $(document).ready(function()
//         {
//             initialize();
//         });

//         // WINDOW RESIZE
//         function doResize() {
//             _private.update();
//         }

//         var resizeTimer;
//         $(window).resize(function() {
//             clearTimeout(resizeTimer);
//             resizeTimer = setTimeout(doResize, 200);

//         });
//     }
// });

// $(function() {
//     $( document ).ready(function()
//     {
//         $(".m-text-modal").each(function(_index, _ell)
//         {
//             var mTextModal = new $.mTextModal($(_ell));
//         });
        
//     });
// });

