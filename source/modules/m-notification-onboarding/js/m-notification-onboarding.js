 jQuery.extend({

    mNotificationOnboarding: function(){

        // PUBLIC
        var _this               = this;

        var _private = {

            // PRIVATE OBJECTS
            notification			: null,
            notifiedIntro			: false,
            notifiedTrigger			: false,
            notifiedTimeline		: false,
            notifiedFacebook		: false,

            // PRIVATE FUNCTIONS
            setup:function()
            {   
	            /*
	            _private.placeNotification("intro", "Klik verder naar de eerste highlights", "right");
	            
	            setTimeout(function() {
	            	_private.placeNotification("timeline", "Of selecteer een highlight uit de timeline", "down");
	            }, 3000);
	            */
            },
            reposition:function()
            {
	            
            },
            placeNotification:function(type, str, arrowDirection)
            {
	            // setTimeout(function() {
		            $('body').append("<div class='m-notification-onboarding m-notification-onboarding__"+type+" notification arrow__"+arrowDirection+"'><h3>"+str+"</h3><div class='arrow'></div>");
	            // }, 500);
	            
	            
	            switch(type)
	            {
		            case 'intro' :
		            	_private.notifiedIntro = true;
		            break;
		            case 'trigger' :
		            	_private.notifiedTrigger = true;
		            break;
		            case 'timeline' :
		            	_private.notifiedTimeline = true;
		            break;
		            case 'facebook' :
		            	_private.notifiedFacebook = true;
		            break;
	            }
            },
            removeNotifications:function()
            {
	            $('body').find('.m-notification-onboarding__intro').remove();
	            $('body').find('.m-notification-onboarding__trigger').remove();
	            $('body').find('.m-notification-onboarding__timeline').remove();
	            $('body').find('.m-notification-onboarding__facebook').remove();
            },
            eventListeners:function() 
            {
                $(document).on( "notificateOnboarding-intro", function( event ) {
                    
                    if(!_private.notifiedIntro){
	                    _private.placeNotification("intro", "Klik verder naar de eerste highlights", "right");
                    }
                });
                $(document).on( "notificateOnboarding-trigger", function( event )
                {
                    if(!_private.notifiedTrigger){
                    	_private.placeNotification("trigger", "Ontdek meer over een highlight", "right");
                    }
                });
                $(document).on( "notificateOnboarding-timeline", function( event )
                {
                	if(!_private.notifiedTimeline){    
                    	_private.placeNotification("timeline", "Of selecteer een highlight uit de timeline", "down");
                    }
                });
                $(document).on( "notificateOnboarding-facebook", function( event )
                {
                    if(!_private.notifiedFacebook){
                    	_private.placeNotification("facebook", "Deel ons publieke jaarverslag op Facebook", "left");
                    }
                });
                $(document).on( "sceneChanged", function( event ) {
                    
                    _private.removeNotifications();
                });
            }
        };
        function initialize()
        {
            _private.setup();   
            _private.eventListeners();
        }
        $(document).ready(function()
        {
            initialize();
        });

        // WINDOW RESIZE
        function doResize() {
            _private.reposition();
        }

        var resizeTimer;
        $(window).resize(function() 
        {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(doResize, 500);
        });
    }
});
$(function() {
    $( document ).ready(function()
    {
            var mNotificationOnboarding = new $.mNotificationOnboarding();
    });
});