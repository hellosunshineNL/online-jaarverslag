(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbool1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Laag 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EEEEEE").s().p("A1iVjQo7o7AAsoQAAsnI7o7QI7o7MnAAQMoAAI7I7QI7I7AAMnQAAMoo7I7Qo7I7soAAQsnAAo7o7g");
	this.shape.setTransform(183.1,183.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbool1, new cjs.Rectangle(-11.9,-11.9,390,390), null);


// stage content:
(lib.canvas1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var scene = this;
		
		document.getElementById("btn-1").addEventListener("click", fl_MouseClickHandler.bind(this));
		
		function fl_MouseClickHandler()
		{
			// Uw aangepaste code starten
			// Deze examplecode geeft de woorden "Aangeklikt met muis" weer in het deelvenster Uitvoer.
			console.log("dom button clicked");
			// Uw aangepaste code beëindigen
			this.gotoAndPlay(51);
			
		}
		
		//Custom event listener - event from dom
		$(document).on( "customEvent_dom", function(e) {
		  console.log("customEvent dom");
		  scene.gotoAndPlay(51);
		});
	}
	this.frame_24 = function() {
		var customEvent = jQuery.Event( "customEvent" );
		$(document).trigger( customEvent );
	}
	this.frame_49 = function() {
		/* Stoppen bij dit frame
		De tijdlijn zal stoppen/pauzeren bij het frame waarin u deze code invoegt.
		U kunt dit ook gebruiken om de tijdlijn van filmclips te stoppen/pauzeren.
		*/
		
		this.stop();
	}
	this.frame_78 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(24).call(this.frame_24).wait(25).call(this.frame_49).wait(29).call(this.frame_78).wait(1));

	// Background
	this.bg_1 = new lib.Symbool1();
	this.bg_1.parent = this;
	this.bg_1.setTransform(193.1,200,1,1,0,0,0,183,183);

	this.timeline.addTween(cjs.Tween.get(this.bg_1).to({x:273.1},49).wait(1).to({regX:183.1,scaleX:0.83,scaleY:0.83,x:273.2},7).to({regY:183.1,scaleX:1,scaleY:1,y:200.1},7).to({regY:183,scaleX:0.83,scaleY:0.83,y:200},7).to({regX:183,scaleX:1,scaleY:1,x:273.1},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(273.1,205.1,390,390);
// library properties:
lib.properties = {
	width: 550,
	height: 400,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;