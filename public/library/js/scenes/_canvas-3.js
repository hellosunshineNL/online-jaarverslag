(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.bg_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Laag 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F5A733").s().p("A3bPoIAA/PMAu3AAAIAAfPg");
	this.shape.setTransform(150,100);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.bg_3, new cjs.Rectangle(0,0,300,200), null);


// stage content:
(lib.canvas3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* Muisklikgebeurtenis
		Wanneer u op een opgegeven symboolinstantie klikt, wordt er een functie uitgevoerd waarin u uw eigen aangepaste code kunt toevoegen.
		
		Instructies:
		1. Voeg uw aangepaste code toe op een nieuwe regel na de onderstaande regel "//  Uw aangepaste code starten".
		De code wordt uitgevoerd wanneer er op de symboolinstantie wordt geklikt.
		*/
		
		this.bg_3.addEventListener("click", fl_MouseClickHandler.bind(this));
		
		function fl_MouseClickHandler()
		{
			// Uw aangepaste code starten
			// Deze examplecode geeft de woorden "Aangeklikt met muis" weer in het deelvenster Uitvoer.
			console.log("click");
			
			var customEvent_2 = jQuery.Event( "customEvent_2" );
			$(document).trigger( customEvent_2 );
			// Uw aangepaste code beëindigen
			
		}
	}
	this.frame_24 = function() {
		/* Stoppen bij dit frame
		De tijdlijn zal stoppen/pauzeren bij het frame waarin u deze code invoegt.
		U kunt dit ook gebruiken om de tijdlijn van filmclips te stoppen/pauzeren.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(24).call(this.frame_24).wait(1));

	// Background
	this.bg_3 = new lib.bg_3();
	this.bg_3.parent = this;
	this.bg_3.setTransform(130,99.7,0.867,0.867,0,0,0,150,100);

	this.timeline.addTween(cjs.Tween.get(this.bg_3).to({x:170},24).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(150,113,260,173.3);
// library properties:
lib.properties = {
	width: 300,
	height: 200,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;